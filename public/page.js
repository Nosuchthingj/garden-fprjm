// Page.js - Every line of js needed for client in the  forums.
// Made by John B, john@nosuchthingj.com
// © The GardenMc 2019
//
// Setting vars
// Shortening window.location.pathname to pageURL, gets the path. EG: /profile/self
var pageURL = window.location.pathname;
// Setting the container, needed to render the html
var container = document.getElementById("content")
    // Setting the auth header, needed for every request sent to the mastodon instance
var auth = localStorage.getItem("auth")
    // Setting the header
var header = "<div class=\"header\"><div class=\"avatar\" id=\"avatar\"></div><h1 id=\"bar\"><span class=\"you highlighted\" id=\"you\">You</span><span class=\"local\"><button class=\"but\" id=\"tagsBut\" onclick=\"tags()\">Tags</button></span><span class=\"toot\" id=\"toot\"><button class=\"toot-but\" onclick=\"toot()\">Toot</button></span><span class=\"federated\"><a href=\"profile.php\">Profile</a></span><span class=\"search\">Search</span></h1></div>"
    // Getting the url params. eg: youtube.com/video?v=test, the params would be v=test
var vars = window.location.search;
// Setting the header for profile
var headerProfile = "<div class=\"header\"><div class=\"avatar\" id=\"avatar\"></div><h1 id=\"bar\"><span class=\"you\" id=\"you\">You</span><span class=\"local\"><button class=\"but\" id=\"tagsBut\" onclick=\"tags()\">Tags</button></span><span class=\"toot\" id=\"toot\"><button class=\"toot-but\" onclick=\"toot()\">Toot</button></span><span class=\"federated highlighted\"><a href=\"profile.php\">Profile</a></span><span class=\"search\">Search</span></h1></div>"


    // Setting up header
function header() {
    // checking if page url is equal to profile or profile profile/self
    if (pageURL === "profile/") {
        container.insertAdjacentHTML('beforeend', headerProfile)
    } else if (pageURL === "profile/self") {
        container.insertAdjacentHTML('beforeend', headerProfile)
    } else {
        container.insertAdjacentHTML('beforeend', header)
    }

    function avatar() {
        var avatarr = document.getElementById("avatar")
        var htmlSTRING = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\" onclick=\"  var avatar = document.getElementById(\"avatarrr\");var self = \"self\";var htmlSTRING = \"<div class=\"menu\"><p><span><button onclick=\"page()\" id=\"page\" class=\"but\">Your page</button></span><br><button onclick=\"notifications()\" class=\"but\">Notifications</button><br><button onclick=\"settings()\" class=\"but\">Settings</button><br><button onclick=\"logout()\" class=\"but\">Logout</button></p></div>\";avatarr.insertAdjacentHTML('beforeend', htmlSTRING);\"/>"
        avatarr.innerHTML = htmlSTRING


    }
}
var page = {
    // adding the auth function
    auth: function() {
        // getting code param
        var auth = vars.replace('?code=', '')
            // making a post request
        jQuery.ajax({
                url: "https://mastodon.social/oauth/token?" + jQuery.param({
                    "code": auth,
                    "client_id": "8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213",
                    "client_secret": "f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9",
                    "grant_type": "authorization_code",
                    "redirect_uri": "http://localhost/auth",
                }),
                type: "POST",

            })
            .done(function(data, textStatus, jqXHR) {
                console.log("HTTP Request Succeeded: " + jqXHR.status);
                console.log(data);
                var auth = data.access_token
                    // saving access token
                localStorage.setItem("auth", "Bearer " + auth)
                var auth = localStorage.getItem("auth")
                console.log(auth)
                    // Getting needed info
                jQuery.ajax({
                        url: "https://mastodon.social/api/v1/accounts/verify_credentials",
                        type: "GET",
                        headers: {
                            "Authorization": localStorage.getItem("auth"),
                        },
                    })
                    .done(function(data, textStatus, jqXHR) {
                        console.log("HTTP Request Succeeded: " + jqXHR.status);
                        console.log(data);
                        // saving needed info
                        localStorage.setItem("avatar", data.avatar)
                        localStorage.setItem("display_name", data.display_name)
                        localStorage.setItem("id", data.id)
                        window.location = "/"
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        console.log("HTTP Request Failed");
                        window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"

                    })
                    .always(function() {
                        /* ... */
                    });
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("HTTP Request Failed");
                window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"
            })
            .always(function() {
                /* ... */
            });
    },
    home: function() {

        header.you()
        things.logged()
        things.avatar()

        // else make a GET request to https://mastodon.social/api/v1/timelines/home needed for timeline
        container.insertAdjacentHTML('beforeend', header)
        jQuery.ajax({
                url: "https://mastodon.social/api/v1/timelines/home",
                type: "GET",
                headers: {
                    "Authorization": localStorage.getItem("auth"),
                },
            })
            .done(function(data, textStatus, jqXHR) {
                console.log("HTTP Request Succeeded: " + jqXHR.status);
                console.log(data);
                // running a function and passing the data var
                renderHTML(data)
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("HTTP Request Failed");
            })
            .always(function() {
                /* ... */
            })

        function renderHTML(data) {
            // setting htmlSTRING to null
            var htmlSTRING = ""

            var top = "-299"
            var six = "600"
                // doing a loop
            for (i = 0; i < data.length; i++) {
                top = +top + +six
                    // setting the htmlSTRING to the toot template
                htmlSTRING += "<div class= \"ttoot" + [i] + "\" style=\"top: " + top + "; left: 550px;\"> <div class=\"toot-header\"><img class=\"avatarr\" width=\"40px\" height=\"40px\" src=\"" + data[i].account.avatar + "\" /> <h1 class=\"toot-poster\">" + data[i].account.display_name + "<h1 class=\"tags\"></h1></div><div class=\"toot-contents\">" + data[i].content + "</div>  <div class=\"toot-readmore\"> <a href=\"http://localhost:8000/post/?id=" + data[i].id + "\" class=\"owo\"> Read more</a></div></div>"
            }
            // putting the html string on the screen
            container.insertAdjacentHTML('beforeend', htmlSTRING)
        }


    },
    editor: function() {
        header.none()
        things.logged()
        things.avatar()
    },
    logout: function() {
      localStorage.removeItem("auth")
      localStorage.removeItem("avatar")
      localStorage.removeItem("display_name")
      localStorage.removeItem("id")
      htmlSTRING = "<h1 class=\"highlighted thegardenBig\">The GardenMc</h1><h1 class=\"lggedOut\">Logged Out.</h1>"
      container.innerHTML = htmlSTRING
    },
    profile: function(id) {
      header.people()
      things.logged()
      things.avatar()

      let stateObj = {
        foo: "bar",
      }
      if (pageURL !== "/profile/") {
        history.replaceState(stateObj, "page 2", "profile/?p=" + id)
      }
      jQuery.ajax({
              url: "https://mastodon.social/api/v1/accounts/" + id,
              type: "GET",
              headers: {
                  "Authorization": localStorage.getItem("auth"),
              },
          })
          .done(function(data, textStatus, jqXHR) {
              console.log("HTTP Request Succeeded: " + jqXHR.status);
              console.log(data);
              // Saving needed data as session values instead of local values
              sessionStorage.setItem("header", data.header)
              sessionStorage.setItem("statuses_count", data.statuses_count)
              sessionStorage.setItem("note", data.note)
              sessionStorage.setItem("display_name", data.display_name)
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
              console.log("HTTP Request Failed");
          })
          .always(function() {
              /* ... */
          });
          var note = sessionStorage.getItem("note")
          unescape(note)
          var name = "\"" + sessionStorage.getItem("display_name") + "\""
          console.log(name)
          var htmlSTRING = "<div class=\"profile-header\"><img src=\"" + sessionStorage.getItem("header") + "\" width=\"1360px\" height=\"500\"/></div><div class=\"profile-info\"><h1 class=\"name highlighted\">" + sessionStorage.getItem("display_name") + "</h1>" + "<div class=\"note\"" + note + "</div><h1 class=\"Posts-profile highlighted\">Posts</h1><h1 class=\"Posts-count\">" + sessionStorage.getItem("statuses_count") + "</h1></div>"
          container.insertAdjacentHTML('beforeend', htmlSTRING)
    },
    settings: function() {
      header.none()
      things.logged()
      things.avatar()
      var id = localStorage.getItem("id")
      let stateObj = {
        foo: "bar",
      }
      if (pageURL !== "/settings/") {
        history.replaceState(stateObj, "page 2", "settings/")
      }
      jQuery.ajax({
              url: "https://mastodon.social/api/v1/accounts/" + id,
              type: "GET",
              headers: {
                  "Authorization": localStorage.getItem("auth"),
              },
          })
          .done(function(data, textStatus, jqXHR) {
              console.log("HTTP Request Succeeded: " + jqXHR.status);
              console.log(data);
              // Saving needed data as session values instead of local values
              sessionStorage.setItem("header", data.header)
              sessionStorage.setItem("statuses_count", data.statuses_count)
              sessionStorage.setItem("note", data.note)
              sessionStorage.setItem("display_name", data.display_name)
          })
          .fail(function(jqXHR, textStatus, errorThrown) {
              console.log("HTTP Request Failed");
          })
          .always(function() {
              /* ... */
          });
          var note = sessionStorage.getItem("note")
          unescape(note)
          var name = "\"" + sessionStorage.getItem("display_name") + "\""
          console.log(name)
          var htmlSTRING = "<div class=\"profile-header\"><img src=\"" + sessionStorage.getItem("header") + "\" width=\"1360px\" height=\"500\" onclick=\"action.replaceImage(header)\"/></div><div class=\"profile-info\"><h1 class=\"name highlighted\">" + sessionStorage.getItem("display_name") + "</h1>" + "<div class=\"note\" onclick=\"action.note()\">" + note + "</div><h1 class=\"Posts-profile highlighted\">Posts</h1><h1 class=\"Posts-count\">" + sessionStorage.getItem("statuses_count") + "</h1></div>"
          container.insertAdjacentHTML('beforeend', htmlSTRING)
    },
    tags: function() {
      header.tags()
      things.logged()
      things.avatar()

    },
    toot: function() {
      header.toot()
      things.logged()
      things.avatar()
      // setting html string as the tooting page
      var htmlSTRING = "<p class=\"channel-info\" id=\"about-tags\">Tags on the forums are like discord channels, setting a tag as #general will post in #general. To set multiple tags just add a space between the tags.<\/p><button id=\"hover\">What are tags?<\/button><textarea class=\"toot-area-channels\" id=\"toot-area-channels\" placeholder=\"Enter tags\"><\/textarea><textarea class=\"toot-area\" id=\"toot-area\"><\/textarea><button class=\"submit\" id=\"submit\" onclick=\"postToot()\">Toot!<\/button>"
          // setting tittle to Toot
      document.title = "Toot"
          // inserting header  and page contents
      container.insertAdjacentHTML('beforeend', htmlSTRING)

    }

}

    // checking if page url is the base
if (pageURL === "/") {
    // detecting if the auth header is null
    if (!auth) {
        // if null send to oauth
        window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"
    } else {
        // else make a GET request to https://mastodon.social/api/v1/timelines/home needed for timeline
        container.insertAdjacentHTML('beforeend', header)
        jQuery.ajax({
                url: "https://mastodon.social/api/v1/timelines/home",
                type: "GET",
                headers: {
                    "Authorization": localStorage.getItem("auth"),
                },
            })
            .done(function(data, textStatus, jqXHR) {
                console.log("HTTP Request Succeeded: " + jqXHR.status);
                console.log(data);
                // running a function and passing the data var
                renderHTML(data)
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("HTTP Request Failed");
            })
            .always(function() {
                /* ... */
            })

        function renderHTML(data) {
            // setting htmlSTRING to null
            var htmlSTRING = ""

            var top = "-299"
            var six = "600"
                // doing a loop
            for (i = 0; i < data.length; i++) {
                top = +top + +six
                    // setting the htmlSTRING to the toot template
                htmlSTRING += "<div class= \"ttoot" + [i] + "\" style=\"top: " + top + "; left: 550px;\"> <div class=\"toot-header\"><img class=\"avatarr\" width=\"40px\" height=\"40px\" src=\"" + data[i].account.avatar + "\" /> <h1 class=\"toot-poster\">" + data[i].account.display_name + "<h1 class=\"tags\"></h1></div><div class=\"toot-contents\">" + data[i].content + "</div>  <div class=\"toot-readmore\"> <a href=\"http://localhost:8000/post/?id=" + data[i].id + "\" class=\"owo\"> Read more</a></div></div>"
            }
            // putting the html string on the screen
            container.insertAdjacentHTML('beforeend', htmlSTRING)
        }
        // getting the avatar and detecting mouse over
        function avatar() {
            var avatarr = document.getElementById("avatar")
            var htmlSTRING = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
            avatarr.innerHTML = htmlSTRING
            var avatar = document.getElementById("avatarrr")
            $("#avatarrr").mouseover(function() {
                var self = "self"
                var htmlSTRING = "<div class=\"menu\"><p><span><button onclick=\"page()\" id=\"page\" class=\"but\">Your page</button></span><br><button onclick=\"notifications()\" class=\"but\">Notifications</button><br><button onclick=\"settings()\" class=\"but\">Settings</button><br><button onclick=\"logout()\" class=\"but\">Logout</button></p></div>"
                avatarr.insertAdjacentHTML('beforeend', htmlSTRING)
                var page = document.getElementById("page")
            })
        }
        avatar()
            // sending person to profile
        function page() {
            window.location = "/profile"
        }
    }
}
// checking if page url is auth
if (pageURL === "/auth") {
    // getting code param
    var auth = vars.replace('?code=', '')
        // making a post request
    jQuery.ajax({
            url: "https://mastodon.social/oauth/token?" + jQuery.param({
                "code": auth,
                "client_id": "8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213",
                "client_secret": "f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9",
                "grant_type": "authorization_code",
                "redirect_uri": "http://localhost/auth",
            }),
            type: "POST",

        })
        .done(function(data, textStatus, jqXHR) {
            console.log("HTTP Request Succeeded: " + jqXHR.status);
            console.log(data);
            var auth = data.access_token
                // saving access token
            localStorage.setItem("auth", "Bearer " + auth)
            var auth = localStorage.getItem("auth")
            console.log(auth)
                // Getting needed info
            jQuery.ajax({
                    url: "https://mastodon.social/api/v1/accounts/verify_credentials",
                    type: "GET",
                    headers: {
                        "Authorization": localStorage.getItem("auth"),
                    },
                })
                .done(function(data, textStatus, jqXHR) {
                    console.log("HTTP Request Succeeded: " + jqXHR.status);
                    console.log(data);
                    // saving needed info
                    localStorage.setItem("avatar", data.avatar)
                    localStorage.setItem("display_name", data.display_name)
                    localStorage.setItem("id", data.id)
                    window.location = "/"
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    console.log("HTTP Request Failed");
                    window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"

                })
                .always(function() {
                    /* ... */
                });
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("HTTP Request Failed");
            window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"
        })
        .always(function() {
            /* ... */
        });
}
// checking page url
if (pageURL === "/profile") {
    // checking auth
    if (!auth) {
        window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"
    } else {
        container.insertAdjacentHTML('beforeend', headerProfile)
            // Getting id of profile
        var auth = vars.replace('?id=', '')
            // Making a GET request
        jQuery.ajax({
                url: "https://mastodon.social/api/v1/accounts/" + auth,
                type: "GET",
                headers: {
                    "Authorization": localStorage.getItem("auth"),
                },
            })
            .done(function(data, textStatus, jqXHR) {
                console.log("HTTP Request Succeeded: " + jqXHR.status);
                console.log(data);
                // Saving needed data as session values instead of local values
                sessionStorage.setItem("header", data.header)
                sessionStorage.setItem("statuses_count", data.statuses_count)
                sessionStorage.setItem("note", data.note)
                sessionStorage.setItem("display_name", data.display_name)
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("HTTP Request Failed");
            })
            .always(function() {
                /* ... */
            });
    }
    // NEED TO UPDATE FOR MODERATION !!!!
    var test = "Nosuchthingj JoshxMeg watf"
    var name = localStorage.getItem("display_name")
    var n = test.indexOf(name)
    console.log(n)
        // Checking if the local storage name value equals a value
    if (["Nosuchthingj", "C00kiez"].indexOf(name) === -1) {
        var note = sessionStorage.getItem("note")
        unescape(note)
            // adding in the moderation option
        var htmlSTRING = "<div class=\"profile-header\"><img src=\"" + sessionStorage.getItem("header") + "\" width=\"1360px\" height=\"500\"/></div><div class=\"profile-info\"><h1 class=\"name highlighted\">" + sessionStorage.getItem("display_name") + "</h1>" + "<div class=\"note\">" + note + "</div><h1 class=\"Posts-profile highlighted\">Posts</h1><h1 class=\"Posts-count\">" + sessionStorage.getItem("statuses_count") + "</h1></div>"
        container.insertAdjacentHTML('beforeend', htmlSTRING)

    } else {
        // Just setting it as normal
        var note = sessionStorage.getItem("note")
        unescape(note)
        var name = "\"" + sessionStorage.getItem("display_name") + "\""
        console.log(name)
        var htmlSTRING = "<div class=\"profile-header\"><img src=\"" + sessionStorage.getItem("header") + "\" width=\"1360px\" height=\"500\"/></div><div class=\"profile-info\"><h1 class=\"name highlighted\">" + sessionStorage.getItem("display_name") + "</h1>" + "<div class=\"note\">" + note + "</div><h1 class=\"Posts-profile highlighted\">Posts</h1><h1 class=\"Posts-count\">" + sessionStorage.getItem("statuses_count") + "</h1><div class=\"moderation\"><button id=\"moderationBut\" onclick=\"moderation(\"test\")\">Moderation</button></div></div>"
        container.insertAdjacentHTML('beforeend', htmlSTRING)

    }
    // Need to complete
    function moderation(a) {
        console.log(a)
    }
    // Getting the avatar
    function avatar() {
        var avatarr = document.getElementById("avatar")
        var htmlSTRING = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
        avatarr.innerHTML = htmlSTRING
        var avatar = document.getElementById("avatarrr")
        $("#avatarrr").mouseover(function() {
            var self = "self"
            var htmlSTRING = "<div class=\"menu\"><p><span><button onclick=\"page()\" id=\"page\" class=\"but\">Your page</button></span><br><button onclick=\"notifications()\" class=\"but\">Notifications</button><br><button onclick=\"settings()\" class=\"but\">Settings</button><br><button onclick=\"logout()\" class=\"but\">Logout</button></p></div>"
            avatarr.insertAdjacentHTML('beforeend', htmlSTRING)
                // setTimeout(function () {
                //   avatarr.innerHTML = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
                // })
            var page = document.getElementById("page")
        })
        $("#avatarrr").mouseout(function() {
            avatarr.innerHTML = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
        })
    }
    document.title = sessionStorage.getItem("display_name");
    avatar()
}
// Checking if page url is /profile/self
if (pageURL === "/profile/self") {
    if (!auth) {
        // sending to oauth
        window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost/auth&scope=read%20write%20follow"
    } else {
        container.insertAdjacentHTML('beforeend', headerProfile)
            // verfiying credentials to make sure nothing is incorrect.
        jQuery.ajax({
                url: "https://mastodon.social/api/v1/accounts/verify_credentials",
                type: "GET",
                headers: {
                    "Authorization": localStorage.getItem("auth"),
                },
            })
            .done(function(data, textStatus, jqXHR) {
                console.log("HTTP Request Succeeded: " + jqXHR.status);
                console.log(data);
                // saving as session storage
                sessionStorage.setItem("header", data.header)
                sessionStorage.setItem("statuses_count", data.statuses_count)
                sessionStorage.setItem("note", data.note)
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("HTTP Request Failed");
                window.location = "https://mastodon.social/oauth/authorize?response_type=code&client_id=8b1b23221b32be1d26e74983d6b490ab26aaaf57a08af2da5954eda5503bd213&client_secret=f6085419af6129da4cf4e2d38b15c049c2f4989545fb637c15962288b2fb32b9&redirect_uri=http://localhost:8000/auth&scope=read%20write%20follow"

            })
            .always(function() {
                /* ... */
            });

        function profileSelf() {
            var note = sessionStorage.getItem("note")
                // setting up the page and url decoding note
            unescape(note)
            var htmlSTRING = "<div class=\"profile-header\"><img src=\"" + sessionStorage.getItem("header") + "\" width=\"1360px\" height=\"500\"/></div><div class=\"profile-info\"><h1 class=\"name highlighted\">" + localStorage.getItem("display_name") + "</h1>" + "<div class=\"note\">" + note + "</div><h1 class=\"Posts-profile highlighted\">Posts</h1><h1 class=\"Posts-count\">" + sessionStorage.getItem("statuses_count") + "</h1></div>"
            container.insertAdjacentHTML('beforeend', htmlSTRING)
        }
        profileSelf()
            // doing the avatar function
        function avatar() {
            var avatarr = document.getElementById("avatar")
            var htmlSTRING = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
            avatarr.innerHTML = htmlSTRING
            var avatar = document.getElementById("avatarrr")
            $("#avatarrr").mouseover(function() {
                var self = "self"
                var htmlSTRING = "<div class=\"menu\"><p><span><button onclick=\"page()\" id=\"page\" class=\"but\">Your page</button></span><br><button onclick=\"notifications()\" class=\"but\">Notifications</button><br><button onclick=\"settings()\" class=\"but\">Settings</button><br><button onclick=\"logout()\" class=\"but\">Logout</button></p></div>"
                avatarr.insertAdjacentHTML('beforeend', htmlSTRING)
                    // setTimeout(function () {
                    //   avatarr.innerHTML = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
                    // })
                var page = document.getElementById("page")
            })
            $("#avatarrr").mouseout(function() {
                avatarr.innerHTML = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
            })
        }
        document.title = localStorage.getItem("display_name");
        avatar()
    }
}
if (pageURL === "/toot") {
    // setting html string as the tooting page
    var htmlSTRING = "<p class=\"channel-info\" id=\"about-tags\">Tags on the forums are like discord channels, setting a tag as #general will post in #general. To set multiple tags just add a space between the tags.<\/p><button id=\"hover\">What are tags?<\/button><textarea class=\"toot-area-channels\" id=\"toot-area-channels\" placeholder=\"Enter tags\"><\/textarea><textarea class=\"toot-area\" id=\"toot-area\"><\/textarea><button class=\"submit\" id=\"submit\" onclick=\"postToot()\">Toot!<\/button>"
        // setting tittle to Toot
    document.title = "Toot"
        // inserting header  and page contents
    container.insertAdjacentHTML('beforeend', htmlSTRING)
    container.insertAdjacentHTML('beforeend', header)
        // running the good ol' avatar function
    function avatar() {
        var avatarr = document.getElementById("avatar")
        var htmlSTRING = "<img src=\"" + localStorage.getItem("avatar") + "\" width=\"100\" height=\"100\" class=\"avatarr\" id=\"avatarrr\"/>"
        avatarr.innerHTML = htmlSTRING
        var avatar = document.getElementById("avatarrr")
        $("#avatarrr").mouseover(function() {
            var self = "self"
            var htmlSTRING = "<div class=\"menu\"><p><span><button onclick=\"page()\" id=\"page\" class=\"but\">Your page</button></span><br><button onclick=\"notifications()\" class=\"but\">Notifications</button><br><button onclick=\"settings()\" class=\"but\">Settings</button><br><button onclick=\"logout()\" class=\"but\">Logout</button></p></div>"
            avatarr.insertAdjacentHTML('beforeend', htmlSTRING)
            var page = document.getElementById("page")
        })
    }
    avatar()
        // setting values
    var container = document.getElementById("updates")
    var header = document.getElementById("bar")
    var auth = localStorage.getItem("auth")
    var infoBut = document.getElementById("hover")
        // post toot function
    function postToot() {
        // getting values oof the text areas
        var tags = $("#toot-area-channels").val();
        var tootContents = $("#toot-area").val();
        // making a post request to the status
        jQuery.ajax({
                url: "https://mastodon.social/api/v1/statuses",
                type: "POST",
                data: {
                    "status": tags + " " + tootContents,
                },
                headers: {
                    // setting header as auth
                    "Authorization": localStorage.getItem("auth"),
                },
            })
            .done(function(data, textStatus, jqXHR) {
                console.log("HTTP Request Succeeded: " + jqXHR.status);
                console.log(data);
                // checking if one of the tags was #suggestion
                if (tags.indexOf("#suggestion") != -1) {
                    // creating a json string for discord
                    var data = "{\"embeds\": [{\"title\": \"New post in suggestions.\",\"color\": 15358260,\"description\":\"" + tootContents + "\",\"footer\": {\"text\": \"posted by " + localStorage.getItem("display_name") + "\"}}]}"
                    console.log(data)
                        // making a POST request to discord webhook
                    var xhr = new XMLHttpRequest();
                    xhr.withCredentials = true;

                    xhr.addEventListener("readystatechange", function() {
                        if (this.readyState === this.DONE) {
                            console.log(this.responseText);
                        }
                    });
                    // posting
                    xhr.open("POST", "https://discordapp.com/api/webhooks/563464838517489686/gqT_X9jjfUgoVtk_oUeTVIxqhw7PG-qlJTbB4egdARXhK-y8xmHIsiYC8RwdKFg8gJRM?=");
                    xhr.setRequestHeader("content-type", "application/json");

                    xhr.send(data);
                }
            })
            // doing the fail
            .fail(function(jqXHR, textStatus, errorThrown) {
                console.log("HTTP Request Failed");
            })
            .always(function() {
                /* ... */
            });
    }
    if (pageURL === "/editor") {
      container.innerHTML = ""
    }

    // detecting avatar mouse over
    $("#hover").mouseover(function() {
        $("#about-tags").css("display", "block");
        $("#toot-area").css("top", "450px");
    });
}
