var express = require('express')
var app = express()
var haml = require('hamljs');
var fs = require('fs');
var moderation = JSON.parse(fs.readFileSync('public/moderation.json', 'utf8'));

app.use(express.static(__dirname + '/public'));

// Setting the url
var url = "http://localhost"
// setting up the / page
app.get('/', function(req, res) {

    // rendering the haml
    var hamlView = fs.readFileSync('public/index.haml', 'utf8');
    res.end(haml.render(hamlView));
});

app.get('/auth', function(req, res) {
    var hamlView = fs.readFileSync('public/index.haml', 'utf8');
    res.end(haml.render(hamlView));
})
app.get('page.js', function(req, res) {
    res.sendFile(__dirname + '/public/page.js')
})
app.get('/editor', function(req, res) {
    // rendering the haml
    var hamlView = fs.readFileSync('public/index.haml', 'utf8');
    res.end(haml.render(hamlView));
})
app.get('/profile', function(req, res) {
  // rendering the haml
  var hamlView = fs.readFileSync('public/index.haml', 'utf8');
  res.end(haml.render(hamlView));
})
app.get('/settings', function(req, res) {
  // rendering the haml
  var hamlView = fs.readFileSync('public/index.haml', 'utf8');
  res.end(haml.render(hamlView));
})
app.get('/toot', function(req, res) {
  // rendering the haml
  var hamlView = fs.readFileSync('public/index.haml', 'utf8');
  res.end(haml.render(hamlView));
})
//When a post request is made to the url it runs a function
app.post('/moderation/approve', function(req, res) {
    var auth = req.get("Authorization")
    var action = req.get("Action")
    var http = require("https")
    // Sending a GET request to the mastodon  api to make sure that the person didn't change the local  storage value
    var options = {
        "method": "GET",
        "hostname": "mastodon.social",
        "port": null,
        "path": "/api/v1/accounts/verify_credentials",
        "headers": {
            "content-length": "0",
            "authorization": auth
        },
        // accepting json
        json: true
    };
    console.log(action)
    var req = http.request(options, function(res) {
        var chunks = [];

        res.on("data", function(chunk) {
            chunks.push(chunk);
        });

        res.on("end", function() {
            //  for,atting
            var body = Buffer.concat(chunks);
            var body = body.toString()
            body = JSON.parse(body)
            // do a for loop
            for (i in moderation.aboveHelper) {
                // If the username is found in the  moderations file it allows you to go onward
                if (moderation.aboveHelper[i].indexOf(body.username) > -1) {
                    console.log("yes")
                }
            }

        })

    });

    // send a 200 status code
    res.sendStatus(200)
    req.end();
})

// app.get('moderation.json', function(req, res) {
//     res.sendFile(__dirname + 'moderation.json')
// })
app.get('/post', function(req, res) {
  var hamlView = fs.readFileSync('public/index.haml', 'utf8');
  res.end(haml.render(hamlView));
})
app.get('/login/auth', function(req, res) {
  var action = req.get("Code")
  console.log(action)
  if(action === "4172") {
    res.json("works");

  } else {
    res.json({"ERROR": "Not logged in"});

  }

})
app.listen(80, function() {
    console.log("started")
})
